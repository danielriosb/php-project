<?php namespace dmrb\Http\Controllers;

class FrontendController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Frontend Controller
	|--------------------------------------------------------------------------
	|
	|
	*/

	/**
	 * Show the application homepage screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('index');
	}
	

}
