<?php namespace dmrb\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Session;
use Redirect;

use dmrb\Http\Requests\ContactFormRequest;
use dmrb\Http\Controllers\Controller;


class ContactController extends Controller {
	
	/*
	|--------------------------------------------------------------------------
	| Contact Controller
	|--------------------------------------------------------------------------
	| This controller sent an email to a contact email address.
	|
	*/

	/*Send email function*/
	public function store(ContactFormRequest $request)
	{
		
		Mail::send('emails.contact', array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
			'subject' => $request->get('subject'),
            'user_message' => $request->get('message')
        ), function($msj)
		{	
			$msj->subject('DMRB Contact');
			$msj->to('danielriosb@gmail.com', 'Admin');
			
		}
		);
		
		echo "SENDING";
	}
		
}


