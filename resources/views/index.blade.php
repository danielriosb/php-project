@extends('frontend.layouts.master')

@section('after-scripts-end')
<!-- Google map location -->
<script>
		(function($) {
  "use strict";

		var styles = [
				        {
				            stylers: [
				                { saturation: 0 }

				            ]
				        },{
				            featureType: 'road',
				            elementType: 'geometry',
				            stylers: [
				                { hue: "#74b7b0" },
				                { visibility: 'simplified' }
				            ]
				        },{
				            featureType: 'road',
				            elementType: 'labels',
				            stylers: [
				                { visibility: 'on' }
				            ]
				        }
				      ],

				       lat = 45.430516, 
				       lng = -75.709224,

				      customMap = new google.maps.StyledMapType(styles,
				          {name: 'Styled Map'}),
				      mapOptions = {
				          zoom: 14,
						scrollwheel: false,
						disableDefaultUI: true,
				          center: new google.maps.LatLng( lat, lng ),
				          mapTypeControlOptions: {
				              mapTypeIds: [google.maps.MapTypeId.ROADMAP]
				          }
				      },
				      map = new google.maps.Map(document.getElementById('map'), mapOptions),
				      myLatlng = new google.maps.LatLng( lat, lng ),




				      marker = new google.maps.Marker({
				        position: myLatlng,
				        map: map,
						icon: {
						            url: 'img/marker.png',
						            scaledSize: new google.maps.Size(36, 58)
						        }
				      });
				      map.mapTypes.set('map_style', customMap);
				      map.setMapTypeId('map_style');

})(jQuery);
	</script>
@stop