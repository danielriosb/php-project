<!DOCTYPE html>

<html>

<head>


	<!-- Metas -->
	<meta charset="utf-8">
	<title>Daniel Rios - CV/RESUME</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}" />
    @yield('meta')


	<!-- Css -->
    @yield('before-styles-end')
	<link href="{{ URL::to('/') }}/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ URL::to('/') }}/css/magnific-popup.css" type="text/css" rel="stylesheet" media="all" />
	<link href="{{ URL::to('/') }}/css/font.css" rel="stylesheet" type="text/css" media="all">
	<link href="{{ URL::to('/') }}/css/base.css" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ URL::to('/') }}/css/main.css" rel="stylesheet" type="text/css" media="all" />
	<link href="{{ URL::to('/') }}/css/owl-carousel/owl.carousel.css" rel="stylesheet" media="all">
	<link href="{{ URL::to('/') }}/css/owl-carousel/owl.theme.css" rel="stylesheet" media="all">
    @yield('after-styles-end')
    <!--@include('includes.partials.mixpanel')-->

</head>

<body>
    

	<!--Wrapper-->
	<div id="wrapper">
        @yield('content')
        
        
		<!--Home section-->
		@include('frontend.includes.homesection') 
		<!--End home section-->
        
		<!--Navigation container-->
		@include('frontend.includes.navigation') 
		<!--End navigation container-->

		<!--About section-->
		@include('frontend.includes.about') 
		<!--End about section-->
        
		<!--Resume section-->
		@include('frontend.includes.resume') 
		<!--End resume section-->
        
		<!--Contact section-->
		@include('frontend.includes.contact') 
		<!--End contact section-->
        
		<!--Footer-->
		@include('frontend.includes.footer') 
		<!--End footer-->

	</div>
	<!--End wrapper-->


	<!--Javascript-->
	<script src="{{ URL::to('/') }}/js/jquery-1.11.3.min.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/owl.carousel.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/jquery.magnific-popup.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/masonry.min.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/jquery.smooth-scroll.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/jquery.appear.min.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/timer.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/placeholders.min.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/spectragram.min.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/easing.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/jquery.ui.totop.min.js" type="text/javascript"></script>
	<script src="{{ URL::to('/') }}/js/script.js" type="text/javascript"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
    
    @yield('before-scripts-end')
	<script type="text/javascript">
		$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
		});
	</script>
	@yield('after-scripts-end')

	<!-- Google analytics -->


	<!-- End google analytics -->


</body>

</html>