<!--Resume section-->
		<section id="resume" class="resume pt-100 pb-80">


			<!--Container-->
			<div class="container ">

				<!--Row-->
				<div class="row">

					<div class="col-md-12">

						<div class="block-title mb-60">
							<h2 class="title">My Resume</h2>
						</div>
					</div>

				</div>
				<!--End row-->

				<!--Row-->
				<div class="row masonry">


					<!--Block content-->
					<div class="block-content">

						<!--Work Experience-->
						<div class="col-md-4 col-sm-6 resume-boxe masonry">

							<div class="inner-resume-boxe">

								<div class="block-icon"><i class="ic-toolbox"></i></div>
								<div class="block-title">
									<h3 class="title medium">Work Experience</h3>
								</div>

								<div class="timeline mt-40">

									<div class="exp">
										<div class="hgroup mb-5">
											<h4 class="positionname">Software Analyst and Developer</h4>
											<h4 class="companyname">Buildmetric (L-Spark)</h4>
											<h6>April 2016 &ndash; <span class="current">Current</span></h6>
										</div>
										<p>Integration between Laravel (PHP Framework) and Web Template (HTML5/CSS3). Programmed ...
										</p>
									</div>

									<div class="exp">
										<div class="hgroup mb-5">
											<h4 class="positionname">Software Developer/Graphic Designer</h4>
											<h4 class="companyname">Llama Communications (Canada)</h4>
											<h6>January &ndash; June 2015</h6>
										</div>
										<p>Designed and developed web applications in a Mac enviroment using(Joomla, Wordpress, MySQL ...

										</p>
									</div>

									<div class="exp">
										<div class="hgroup mb-5">
											<h4 class="positionname">Web Analyst and Developer/Graphic Designer</h4>
											<h4 class="companyname">IT Experts (Canada)</h4>
											<h6>May &ndash; June, 2014</h6>
										</div>
										<p>Improve the maintain efficiency by reconstructing official website with modern technologies and framework ...

										</p>
									</div>

									<div class="exp">
										<div class="hgroup mb-5">
											<h4 class="positionname">Software Analyst/Developer</h4>
											<h4 class="companyname">O’BOIS International (Canada)</h4>
											<h6>October, 2013 &ndash; March, 2014</h6>
										</div>
										<p>Designed, developed and implemented company intranet website for time management of employees (Visual Basic .NET,
											MS Access, Excel, MVC, IIS) ...
										</p>
									</div>

									<div class="exp">
										<div class="hgroup mb-5">
											<h4 class="positionname">Functional Analyst/Web Developer</h4>
											<h4 class="companyname">Freelance (Canada)</h4>
											<h6>January &ndash; October, 2013</h6>
										</div>
										<p>Provided technical support, developed SAP R/3 solutions using ABAP, and consulted ...
										</p>
									</div>

									<div class="exp">
										<div class="hgroup mb-5">
											<h4 class="positionname">Functional Analyst SAP PP and ABAP Developer (Consulting)</h4>
											<h4 class="companyname">Chain Services TI (SAP Projects)</h4>
											<h6>February, 2010 &ndash; August, 2012</h6>
										</div>
										<p>Provided technical support, developed SAP R/3 solutions using ABAP on different modules (PP, FI, CO, SD, HR) and
											team member on 1 SAP PP implementation as Junior SAP PP consultant...

										</p>
									</div>

								</div>
							</div>

						</div>
						
						<!--Education-->
						<div class="col-md-4 col-sm-6 resume-boxe masonry">


							<div class="inner-resume-boxe">

								<div class="block-icon"><i class="ic-pencil"></i></div>
								<div class="block-title">
									<h3 class="title medium">Education</h3>
								</div>

								<div class="timeline mt-40">

									<div class="edu">
										<div class="hgroup mb-5">
											<h4 class="positionname">Computer Science Diploma </h4>
											<h4 class="companyname">CIBERTEC University of Applied Sciences Lima, Peru (3 Years)</h4>
											<h6>2006 &ndash; Nov 2009</h6>
										</div>
										<p>

										</p>
									</div>

								</div>
							</div>

						</div>
						
						<!--Technicall Skills-->
						<div class="col-md-4 col-sm-6 resume-boxe masonry">


							<div class="inner-resume-boxe">

								<div class="block-icon"><i class="ic-wallet"></i></div>
								<div class="block-title">
									<h3 class="title medium">Technical Skills</h3>
								</div>

								<div class="ast-list mt-40 ">

									<ul>
										<li>
											<span>Java</span>
											<span>VB.NET</span>
											<span>Abap IV</span>
										</li>

										<li>
											<span>MVC</span>
											<span>SOAP</span>
											<span>JavaScrip</span>
										</li>

										<li>
											<span>SAP</span>
											<span>HTML5</span>
											<span>CSS3</span>
										</li>
										<li>
											<span>Web Services</span>
											<span>MySQL</span>
											<span>ORACLE</span>
										</li>
										<li>
											<span>ASPX, ASP</span>
											<span>Struts</span>
											<span>OOP</span>
										</li>
										
										<li>
											<span>GIT</span>
											<span>Angular.JS</span>
											<span>Laravel</span>
										</li>
										
										<li>
											<span>Cordova</span>
											<span>JSON</span>
											<span>JQuery</span>
										</li>


									</ul>

								</div>



							</div>

						</div>
						
						<!--Courses-->
						<div class="col-md-4 col-sm-6 resume-boxe masonry">


							<div class="inner-resume-boxe">

								<div class="block-icon"><i class="ic-pencil"></i></div>
								<div class="block-title">
									<h3 class="title medium">Courses</h3>
								</div>

								<div class="timeline mt-40">
									
									<div class="edu">
										<div class="hgroup mb-5">
											<h4 class="positionname">Complete Android Developer Course</h4>
											<h4 class="companyname">Udemy.com</h4>
											<h6>January 2016 &ndash; March 2016</h6>
										</div>
										<p>

										</p>
									</div>

									<div class="edu">
										<div class="hgroup mb-5">
											<h4 class="positionname">Create Android and iOS Apps Using HTML, CSS, and JS</h4>
											<h4 class="companyname">Udemy.com</h4>
											<h6>December 2015 &ndash; January 2016</h6>
										</div>
										<p>

										</p>
									</div>


									<div class="edu">
										<div class="hgroup mb-5">
											<h4 class="positionname">Mobile Apps Development</h4>
											<h4 class="companyname">accredited by the Complutense University of Madrid</h4>
											<h6>November &ndash; December 2015</h6>
										</div>
										<p>

										</p>
									</div>

									<div class="edu">
										<div class="hgroup mb-5">
											<h4 class="positionname">Software Architecture Design Patterns in Java</h4>
											<h4 class="companyname">Willis College</h4>
											<h6>September &ndash; December 2015</h6>
										</div>
										<p>

										</p>
									</div>
									
									<div class="edu">
										<div class="hgroup mb-5">
											<h4 class="positionname">French Language Course</h4>
											<h4 class="companyname">University of Quebec</h4>
											<h6>September 2012 &ndash; June 2013</h6>
										</div>
										<p>

										</p>
									</div>
									
									<div class="edu">
										<div class="hgroup mb-5">
											<h4 class="positionname">SAP PP Course</h4>
											<h4 class="companyname">Chain Services TI</h4>
											<h6>October &ndash; December 2010</h6>
										</div>
										<p>

										</p>
									</div>
									
									<div class="edu">
										<div class="hgroup mb-5">
											<h4 class="positionname">SAP ABAP Course</h4>
											<h4 class="companyname">Chain Services TI</h4>
											<h6>February &ndash; May 2010</h6>
										</div>
										<p>

										</p>
									</div>


								</div>
							</div>

						</div>
						
						<!--Languages-->
						<div class="col-md-4 col-sm-6 resume-boxe masonry">

							<div class="inner-resume-boxe">

								<div class="block-icon"><i class="ic-mic"></i></div>
								<div class="block-title">
									<h3 class="title medium">Languages</h3>
								</div>

								<div class="skills mt-40 ">

									<ul class="skill-list clearfix">
										<li>
											<h4>Spanish (Advanced)</h4>
											<div class="rating">
												<span></span>
												<span></span>
												<span></span>
												<span></span>
												<span></span>
											</div>
										</li>
										<li>
											<h4>English (Advanced)</h4>
											<div class="rating">
												<span></span>
												<span></span>
												<span></span>
												<span></span>
												<span></span>
											</div>
										</li>
										<li>
											<h4>French (Advanced)</h4>
											<div class="rating">
												<span></span>
												<span></span>
												<span></span>
												<span></span>
												<span></span>
											</div>
										</li>
										<li>
											<h4>German (Intermediate)</h4>
											<div class="rating">
												<span></span>
												<span></span>
												<span class="transparent"></span>
												<span class="transparent"></span>
												<span class="transparent"></span>
											</div>
										</li>
										<li>
											<h4>Portuguese (Basic)</h4>
											<div class="rating">
												<span></span>
												<span class="transparent"></span>
												<span class="transparent"></span>
												<span class="transparent"></span>
												<span class="transparent"></span>
											</div>
										</li>

									</ul>

								</div>

							</div>

						</div>
						
						<!--Interests-->
						<div class="col-md-4 col-sm-6 resume-boxe masonry">


							<div class="inner-resume-boxe">

								<div class="block-icon"><i class="ic-telescope"></i></div>
								<div class="block-title">
									<h3 class="title medium">Interests</h3>
								</div>

								<div class="hb-list mt-40 ">

									<ul>
										<li>
											<span><i class="icon-soccer-ball"></i></span>
											<h6>Sports</h6>
										</li>

										<li>
											<span><i class="ic-megaphone"></i></span>
											<h6>Music</h6>
										</li>


										<li>
											<span><i class="ic-hotairballoon"></i></span>
											<h6>Travel</h6>
										</li>



										<li>
											<span><i class="ic-book-open"></i></span>
											<h6>Reading</h6>
										</li>


									</ul>

								</div>

							</div>

						</div>
						
						<!--Assets-->
						<div class="col-md-4 col-sm-6 resume-boxe masonry">


							<div class="inner-resume-boxe">

								<div class="block-icon"><i class="ic-wallet"></i></div>
								<div class="block-title">
									<h3 class="title medium">Assets</h3>
								</div>

								<div class="ast-list mt-40 ">

									<ul>
										<li>
											<span>Responsible</span>
											<span>Diligence</span>
											<span>Labour</span>
										</li>

										<li>
											<span>Rigor</span>
											<span>Creative</span>
											<span>Funny</span>
										</li>

										<li>
											<span>Great Communicator</span>
											<span>Flexible</span>
										</li>


									</ul>

								</div>



							</div>

						</div>

					</div>
					<!--End block content-->

				</div>
				<!--End row-->

			</div>
			<!--End container-->

			<!--Block content-->
			<div class="block-content clearfix  t-center pt-60 pb-60">

				<!--Container-->
				<div class="container ">

					<!--Row-->
					<div class="row">


						<div class="block-client clearfix ">

							<ul id="client-carousel">
								<li><img src="{{ URL::to('/') }}/img/logo/sap.png" alt=""></li>
								<li><img src="{{ URL::to('/') }}/img/logo/java.png" alt=""></li>
								<li><img src="{{ URL::to('/') }}/img/logo/androidapple.png" alt=""></li>
								<li><img src="{{ URL::to('/') }}/img/logo/oracle.png" alt=""></li>
								<li><img src="{{ URL::to('/') }}/img/logo/bootstrap.png" alt=""></li>
								<li><img src="{{ URL::to('/') }}/img/logo/webcodes.png" alt=""></li>
								<li><img src="{{ URL::to('/') }}/img/logo/jquery.png" alt=""></li>

							</ul>

						</div>

					</div>
					<!--End row-->

				</div>
				<!--End container-->


			</div>
			<!--End block content-->
		</section>
		<!--End resume section-->