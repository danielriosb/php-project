<!--Contact section-->
<section id="contact" class="contact">

    <!--Block content-->
    <div class="block-content">


        <!--Block contact-->
        <div class="block-contact">
            <div id="map">

            </div>

            <div class="contact-overlay bg-grey-dark">

                <div class="block-title mb-60">
                    <h2 class="title white">Contact me</h2>
                </div>

                <div class="block-form">
                    <!--Contact form -->          
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    
                    {!! Form::open(array('action' => 'ContactController@store','class'=>'contact-form clearfix')) !!}
                        {!! Form::text('name', null, 
                            array('required', 
                                'id'=>'name', 
                                'placeholder'=>'Name *')) !!}

                        {!! Form::text('email', null, 
                            array('required', 
                                'id'=>'email', 
                                'placeholder'=>'Email *')) !!}
                                
                        {!! Form::text('subject', null, 
                            array('id'=>'subject', 
                                'placeholder'=>'Subject')) !!}

                        {!! Form::textarea('message', null, 
                            array('required', 
                                'id'=>'message', 
                                'placeholder'=>'Message *')) !!}



                        {!! Form::submit('Contact me!', 
                        array('class'=>'but opc white submit')) !!}
                    {!! Form::close() !!}

                    <!--Contact form message-->
                    <div id="success">
                        <h2>Your message has been sent. Thank you!</h2></div>
                    <div id="error">
                        <h2>Sorry your message can not be sent.</h2></div>
                    <!--End contact form message-->

                </div>

            </div>

        </div>
        <!--End block contact-->

    </div>
    <!--End block content-->

    <!--Block content-->
    <div class="block-content bg-grey pt-80 pb-80">

        <!--Container-->
        <div class="container">

            <!--Row-->
            <div class="row">


                <div class="block-info-holder clearfix">

                    <div class="col-sm-4">

                        <div class="block-info">
                            <div class="info-ico">
                                <span class="ico">
										<i class="ic-phone"></i>
										</span>
                            </div>


                            <div class="info-det">
                                <h5 class="mb-10">Call Me</h5>
                                <p>Phone : + 1 819 635-8074</p>
                            </div>


                        </div>

                    </div>

                    <div class="col-sm-4">

                        <div class="block-info">
                            <div class="info-ico">
                                <span class="ico">
										<i class="ic-clock"></i>
										</span>
                            </div>

                            <!--Work Hours-->
                            <div class="info-det">
                                <h5 class="mb-10">Work hours</h5>
                                <p>
                                    Monday - Friday : 09am - 18pm
                                    <br> Saturday : 09am - 1pm
                                </p>
                            </div>


                        </div>

                    </div>

                </div>

            </div>
            <!--End row-->

        </div>
        <!--End container-->


    </div>
    <!--End block content-->

</section>
<!--End contact section-->