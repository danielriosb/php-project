<!--Home section-->
		<section id="home" class="home">


			<!--Block content-->
			<div class="block-content ">


				<div class="block-teaser block-background-image large overlay" data-background="{{ URL::to('/') }}/img/teaser/bg-homepage.jpg">

					<!--Container-->
					<div class="container ">


						<!--Row-->
						<div class="row">


							<div class="col-sm-12 ">
								<h1 class="white ">Daniel Rios</h1>
								<h3 class="mt-20 white mb-200">Software Analyst / Developer</h3>
							</div>

						</div>
						<!--End row-->

						<!--Row-->
						<div class="row">


							<div class="col-sm-6 ">
								<a href="download/Resume_Daniel_Rios.pdf" class="but opc ico white"><i class="icon-print-1"></i>Print Out My CV</a>
								<h6 class="extext whitetext">* for the Word version, please contact me.</h6>
							</div>


							<div class="col-sm-6 t-right">
								<p class="white short-email">Do you have any questions ? Write to me at <a href="mailto:danielriosb@gmail.com">danielriosb@gmail.com</a> </p>
							</div>

						</div>
						<!--End row-->

					</div>
					<!--End container-->
				</div>
			</div>
			<!--End block content-->

		</section>
		<!--End home section-->