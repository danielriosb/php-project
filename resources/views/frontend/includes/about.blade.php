<!--About section-->
		<section id="about" class="about">

			<!--Block content-->
			<div class="block-content clearfix">

				<div class="block-about one bg-grey-1 ">

					<div class="block-title">
						<h2 class="title">Who am I ?</h2>
					</div>

					<h5 class="lead mt-40">A talented Software Analyst / Developer</h5>

					<p class="mt-20">Over 4 years of experience working closely with clientele focused on software production, development, implementation
						and support. Specialized skills set as a Developer/Consultant on web, desktop and mobile applications on multiple platforms.
						Upgraded knowledge of Software Architecture Design Patterns in Java, plus an accreditation on Mobile Apps development.</p>

					<a href="download/Resume_Daniel_Rios.pdf" class="but brd ico white mt-30"><i class="icon-down-circled2 "></i>Download My CV</a>
					<h6 class="extext">* for the Word version, please contact me.</h6>

				</div>

				<div class="block-about two bg-grey-2 ">

					<div class="block-title">
						<h2 class="title">Personal Info</h2>

					</div>

					<ul class="mt-40 info">
						<li><span>Birthdate</span> : 21/05</li>
						<li><span>Phone</span> : + 1 819 635 8074</li>
						<li><span>Email</span> : danielriosb@gmail.com</li>
						<li><span>Skype</span> : danielriosb </li>
						<li><span>Freelance</span> : Available</li>
						<li><span>Adresse</span> : Gatineau, Quebec, Canada</li>
					</ul>

				</div>


				<div class="block-about three bg-grey-1">

					<div class="block-title">
						<h2 class="title">My Expertise</h2>
					</div>

					<div class="block-expertise mt-40">


						<!--Expertise-->
						<div class="expertise mb-20 clearfix">

							<div class="exp-ico">
								<span class="ico">
									<i class="ic-desktop"></i>
									</span>
							</div>

							<div class="exp-det">
								<h6>Desktop/Web applications</h6>
								<p>Desktop/Web application development, including cloud-based applications.</p>
							</div>

						</div>
						<!--Expertise-->



						<!--Expertise-->
						<div class="expertise mb-20 clearfix">

							<div class="exp-ico">
								<span class="ico">
									<i class="ic-phone"></i>
									</span>
							</div>

							<div class="exp-det">
								<h6>Mobile Applications</h6>
								<p>Applications for Android, Windows Mobile and iOS (iPhone and iPad).</p>
							</div>

						</div>
						<!--Expertise-->


						<!--Expertise-->
						<div class="expertise clearfix">

							<div class="exp-ico">
								<span class="ico">
									<i class="ic-lock"></i>
									</span>
							</div>

							<div class="exp-det">
								<h6>Sap Consulting</h6>
								<p>Experience as SAP ABAP and SAP PP.(1 Implementation)</p>
							</div>

						</div>
						<!--Expertise-->


					</div>

				</div>

			</div>
			<!--End block content-->

		</section>
		<!--End about section-->