<!--Footer-->
		<footer class="footer t-center pt-60 pb-60">

			<!--Container-->
			<div class="container">

				<!--Row-->
				<div class="row">

					<div class="col-md-12">

						<hr>
						<!--Copyrigh-->
						<p class="pt-20">&copy; 2016 all rights reserved. A product of <a href="http://www.dmrb.tech" target="_blank">DMRB.TECH</a>. This Webside is made with <a href="https://laravel.com" target="_blank">Laravel Framework</a>.</p>

					</div>

				</div>
				<!--End row-->

			</div>
			<!--End container-->


		</footer>
		<!--End footer-->